const express = require('express');
const morgan = require('morgan')
const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
global.Usage = require('./models/usageModel');
const  Routes = require('./routes/usageRoutes');

mongoose.connect(
  'mongodb://localhost/DatadogOrgUsage' ,
  { useNewUrlParser: true }
);
const port = process.env.PORT || 3000;
const app = express();
app.use(cors());
app.use(bodyParser.urlencoded({limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));
app.use(express.json());
app.use(morgan("dev"));

Routes(app);
app.listen(port);



app.use((req, res) => {
  res.status(404).send({ url: `${req.originalUrl} not found` });
});

console.log(`Server started on port ${port}`);