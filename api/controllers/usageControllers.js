const mongoose = require('mongoose');
const usage = mongoose.model('usage');


exports.list_all_data = (req , res) => {
    usage.find({}, (err, usages) => {
        if (err) res.send(err);
        res.json(usages)
    })
}