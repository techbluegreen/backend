const usageBuilder = require('../controllers/usageControllers')

module.exports = app => {
    app
        .route('/usage')
        .get(usageBuilder.list_all_data)
}