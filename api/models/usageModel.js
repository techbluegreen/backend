const mongoose = require('mongoose')
const { Schema } = mongoose;
const usageSchema = new Schema(
    {
        parent_org   : {
            type: String
        },
        org_name   : {
            type: String
        },
        month   : {
            type: String
        },
        infra_host   : {
            type: Number
        },
    },
    { collection: 'SUB_ORG' }
)

module.exports = mongoose.model('usage' , usageSchema)